<?php

namespace App\Validation;

use Respect\Validation\Validator as Respect;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator
{
    protected $errors;
    public function validate($request, array $rules) {
        //d($rules);
        foreach ($rules as $field => $rule) {
            try{
                $rule->setName(ucfirst($field))->assert($request->getParam($field));
            } catch (NestedValidationException $e) {
                $this->errors[$field] = $e->getMessages();
            }
        }
        //if(count($this->errors)==0)
        //    $_SESSION['errors'] = null;
        //else
            $_SESSION['errors'] = $this->errors;
        
        //d($this->errors);
        return $this;
    }
    
    public function failed() 
    {
        //d(!empty($this->errors));
        return empty($this->errors);
    }
}