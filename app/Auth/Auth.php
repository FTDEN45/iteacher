<?php

namespace App\Auth;

use App\Models\User;

class Auth
{
    public function user()
    {
        //d(User::find($_SESSION['user']));
        if(isset($_SESSION['user'])){
       // $User1= array();
        //$User['id'] = User::find($_SESSION['user'])->id;
        //$User1['name'] = User::find($_SESSION['user'])->name;
        //$User1['password'] = User::find($_SESSION['user'])->password;
        //$User1['email'] = User::find($_SESSION['user'])->email;
            return User::find($_SESSION['user']);
        }
        else 
            return false;
    }
    
    public function check() 
    {
        //echo 'xxxxxxxxxxxxxxxxx';
        //d($_SESSION['user']);
        return isset($_SESSION['user']);
        //return TRUE;
    }
    
    public function attempt($email,$password) 
    {
        $user = User::where('email','=', $email)->first();
        //d($user);
        if(!$user){
            return false;
        }
        //d(password_verify($password, $user->password),0);
        if(password_verify($password, $user->password)){
            $_SESSION['user'] = $user->id;
            //d($_SESSION['user'],0);
            return true;
        }
        
        return false;
        
    }
    
    public function logout() 
    {
        unset($_SESSION['user']);
    }
}