<?php

namespace App\Controllers;

use App\Models\User;
use Slim\Views\Twig as View;

class HomeController extends Controller
{
    
    public function index($request, $response)
    {
        //var_dump($request->getParam('name'));
        //var_dump($this->view);
        //return 'Home controller';
        
        //$user = $this->db->table('users')->where('id',1);
        //$user = $this->db->table('users')->find(1);
        //->where('votes', '>', 100)->get()
        //$user = User::find(10);
        //d($user);
        //$user = User::where('email','=','alex@mail.ru')->get()->first();
        //var_dump($user);
        //die();
        
        //User::create([
        //    'name' => 'Denis Kolosov',
        //    'email' => 'ftden@mail.ru',
        //    'password' => '123',
            
        //]);
        
        //$this->flash->addMessage('info','Test flash message');
        //$this->flash->addMessage('error','Test flash message');
        
        return $this->view->render($response, 'home.twig');
    }
}