<?php

namespace App\Controllers\Auth;

use App\Models\User;
use App\Controllers\Controller;
use Respect\Validation\Validator as v;

class AuthController extends Controller
{
    public function getSignOut($request, $response) 
    {
        $this->auth->logout();
        
        return $response->withRedirect($this->router->pathFor('home'));
    }
    
    public function getSignIn($request, $response) 
    {
        return $this->view->render($response, 'auth/signin.twig');
    }
    
    public function postSignIn($request, $response) 
    {
        $auth = $this->auth->attempt(
            $request->getParam('email'),
            $request->getParam('password')
        );
        //d($auth);
        
        if(!$auth){
            $this->flash->addMessage('error','Could not sign you in with those details.');
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }
        
        return $response->withRedirect($this->router->pathFor('home'));
    }
    
    public function getSignUp($request, $response) 
    {   
        //d($request->getAttribute('csrf_value'),0);
        
        return $this->view->render($response, 'auth/signup.twig');
    }
    
    public function postSignUp($request, $response) 
    {
        //var_dump($request->getParams());
        
        $validation = $this->validator->validate($request,[
            'email' =>  v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'name' =>   v::notEmpty()->alpha(),
            'password' => v::noWhitespace()->notEmpty(),
        ]);
        //d($validation,0);
        //d($validation->failed());
        if(!$validation->failed()){
            //redirect back
            return $response->withRedirect($this->router->pathFor('auth.signup'));
        }
        
        User::create([
            'email' => $request->getParam('email'),
            'name' => $request->getParam('name'),
            'password' => password_hash($request->getParam('password'),PASSWORD_DEFAULT/*,['cost'=>10]*/),
        ]);
        
        $this->flash->addMessage('info','You have been signed up!');
        //$this->flash->addMessage('error','Test flash message');
        
        $this->auth->attempt($request->getParam('email'), $request->getParam('password'));
        
        return $response->withRedirect($this->router->pathFor('home'));
    }
}